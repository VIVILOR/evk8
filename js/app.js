var labelTextClass = 'cjaf-text cjaf-text-font cjaf-main-color';


var taskContent = {
    steps: [
        {
            type: 'cjaf-label-swap',
            content: [
                [
                    [
                        {
                            _tag:       'img',
                            _src:       'res/1.png',
                            _hspace:    '10%',
                            _width:     '80%',
                            _vspace:    '10px'
                        },
                        {
                            _tag:   'span',
                            _class: 'evk8-label-text-center evk8-label-text cjaf-text cjaf-text-font cjaf-main-color',
                            _html:  'Homo rudolfensis'
                        }
                    ],
                    [
                        {
                            _tag:       'img',
                            _src:       'res/2.png',
                            _hspace:    '10%',
                            _width:     '80%',
                            _vspace:    '10px'
                        },
                        {
                            _tag:   'span',
                            _class: 'evk8-label-text-center evk8-label-text cjaf-text cjaf-text-font cjaf-main-color',
                            _html:  'Homo habilis'
                        }
                    ]
                ],
                [
                    [
                        {
                            _tag: 'span',
                            _class: 'evk8-label-text cjaf-text cjaf-text-font cjaf-main-color',
                            _html: '2,4–1,85 млн. л. н.'
                        },
                        {
                            _tag:   'hr',
                            _class: 'evk8-delimiter cjaf-main-border-color'
                        },
                        {
                            _tag: 'span',
                            _class: 'evk8-label-text cjaf-text cjaf-text-font cjaf-main-color',
                            _html: 'Кения, Малавия, <i>возможно</i> Эфиопия'
                        }
                    ],
                    [
                        {
                            _tag: 'span',
                            _class: 'evk8-label-text cjaf-text cjaf-text-font cjaf-main-color',
                            _html: '1,85–1,65 млн. л. н.'
                        },
                        {
                            _tag:   'hr',
                            _class: 'evk8-delimiter cjaf-main-border-color'
                        },
                        {
                            _tag: 'span',
                            _class: 'evk8-label-text cjaf-text cjaf-text-font cjaf-main-color',
                            _html: 'Танзания, Кения, Эфиопия'
                        }
                    ]
                ],
                [
                    [
                        {
                            _tag: 'span',
                            _class: 'evk8-label-text cjaf-text cjaf-text-font cjaf-main-color',
                            _html: '\
                                Череп и зубы отличаются крупными размерами.\
                                Лицо заметно более уплощенное, чем у грацильных австралопитеков.\
                            '
                        }
                    ],
                    [
                        {
                            _tag: 'span',
                            _class: 'evk8-label-text cjaf-text cjaf-text-font cjaf-main-color',
                            _html: '\
                                Череп крупнее черепов грацильных австралопитеков,\
                                но мельче по сравнением с черепом другого вида «ранних Homo».\
                                Эндокран мало отличается от австролопитеков,\
                                хотя отмечается тенденция к развитию зоны Брока.\
                            '
                        }
                    ]
                ],
                [
                    [
                        {
                            _tag: 'span',
                            _class: 'evk8-label-text cjaf-text cjaf-text-font cjaf-main-color',
                            _html: '\
                                Посткраниальный скелет по строению похож на скелет\
                                современного человека, но отличается пропорциями,\
                                например, удлиненными руками. Рост около 1,5 метров.\
                            '
                        }
                    ],
                    [
                        {
                            _tag: 'span',
                            _class: 'evk8-label-text cjaf-text cjaf-text-font cjaf-main-color',
                            _html: '\
                                Коренастое существо с удлиненными руками ростом 1–1,25 м.\
                            '
                        }
                    ]
                ],
                [
                    [
                        {
                            _tag: 'span',
                            _class: 'evk8-label-text cjaf-text cjaf-text-font cjaf-main-color',
                            _html: 'Потомки грацильных австралопитеков (скорее всего, <i>A. afarensis</i>).'
                        }
                    ],
                    [
                        {
                            _tag: 'span',
                            _class: 'evk8-label-text cjaf-text cjaf-text-font cjaf-main-color',
                            _html: 'Скорее всего, предки <i>H. ergaster</i>, первых представителей группы архантропов.'
                        }
                    ]
                ]
            ],
            settings: {
                styles: [
                    'width: 192px; margin-right: 8px',
                    'width: 123px; margin-right: 8px',
                    'width: 210px; margin-right: 8px',
                    'width: 192px; margin-right: 8px',
                    'width: 163px'
                ],
                delimiter: [
                    {
                        _tag: 'img',
                        _src: 'res/3.png',
                        _width: '20px',
                        _class: 'evk8-step1-label-delimiter-img'
                    }
                ]
            },
            header: [
                {
                    _tag:   'h2',
                    _html:  'Сопоставьте названия видов и их характеристики',
                    _class: 'cjaf-subheader cjaf-subheader-font'
                }
            ]
        }
    ],
    header: [
        {
            _tag:   'h1',
            _html:  'Задание 1',
            _class: 'cjaf-header cjaf-header-font'
        }
    ],
    validator: {
        button: {
            restart: [
                {
                    _tag:   'div',
                    _class: '\
                        evk8-button-renew\
                        cjaf-cursor-pointer\
                        cjaf-absolute'
                }
            ],
            validate: [
                {
                    _tag:   'div',
                    _class: '\
                        cjaf-button\
                        cjaf-button-normal\
                        cjaf-inline\
                        cjaf-absolute\
                        cjaf-cursor-pointer\
                        cjaf-center\
                        cjaf-transition',
                    _html: 'Проверить'
                }
            ],
            continue: [
                {
                    _tag:   'div',
                    _class: '\
                        cjaf-button\
                        cjaf-button-normal\
                        cjaf-inline\
                        cjaf-absolute\
                        cjaf-cursor-pointer\
                        cjaf-center\
                        cjaf-transition',
                    _html: 'Продолжить'
                }
            ]
        },
        window: {
            modal: {
                text: [
                    {
                        _tag:   'div',
                        _class: 'cjaf-validator-modal-window-header-text',
                        _html:  'Правильно!'
                    },
                    {
                        _tag:   'div',
                        _class: 'cjaf-validator-caption-text cjaf-text cjaf-text-font cjaf-text-color',
                        _html:  'Задание выполнено верно. Можно двигаться дальше.',
                        _style: 'line-height: 24px'
                    }
                ],
                button: {
                    restart: [
                        {
                            _class: '\
                                cjaf-button\
                                cjaf-button-light\
                                cjaf-validator-custom-margin\
                                cjaf-inline\
                                cjaf-cursor-pointer\
                                cjaf-center\
                                cjaf-transition\
                            ',
                            _tag: 'div',
                            _html: 'Вернуться к решению'
                        }
                    ]
                }
            },
            popUp: {
                text: {
                    wrong: [
                        {
                            _tag: 'div',
                            _class: '\
                                cjaf-validator-caption-text\
                                cjaf-text\
                                cjaf-text-font\
                                cjaf-text-color\
                                cjaf-validator-wrong-answer-text-color\
                            ',
                            _html: 'Есть некоторые ошибки. Постарайся их исправить.'
                        }
                    ],
                    correct: [
                        {
                            _tag: 'div',
                            _class: '\
                                cjaf-validator-caption-text\
                                cjaf-text\
                                cjaf-text-font\
                                cjaf-text-color\
                            ',
                            _html: 'Все верно!'
                        }
                    ]
                }
            }
        }
    }
};

var evk8_app = new Vue({
    el: "#evk8-2-0-app",
    data: {
        content: taskContent
    }
});