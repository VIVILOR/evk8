Vue.component('cjaf-controller', {
    template: '\
        <div :class="prefix + \'-app-wrapper\'">\
            <div :class="prefix + \'-work-field\'"\
                <div\
                    v-if="showPopUpWindow"\
                    class="cjaf-overlay"\
                    @click="onValidationButtonClick">\
                </div>\
                <cjaf-base\
                    :content="content.validator.button.restart"\
                    @click.native="activeStep === 1 ? reset() : activeStep = 1">\
                </cjaf-base>\
                <cjaf-base :content="content.header"></cjaf-base>\
                <slot name="customHtml"></slot>\
                <div\
                    :class  ="prefix + \'-steps-widget\'"\
                    :id     ="prefix + \'-steps-widget\'">\
                    <ul v-if="stepsCount > 1">\
                        <li \
                            is          ="cjaf-base"\
                            v-for       ="i in content.data.length"\
                            :content    ="stepLabel(i)"\
                            :class      ="stepLabelClass(i)"\
                        ></li>\
                    </ul>\
                </div>\
                <template v-for="(step, i) in content.steps">\
                    <cjaf-base :content="step.header"></cjaf-base>\
                    <component\
                        v-if        ="i+1 === activeStep"\
                        :is         ="step.type"\
                        :ref        ="\'step\' + (i+1)"\
                        :prefix     ="prefix + \'-step\' + (i+1)"\
                        :content    ="step.content"\
                        :settings   ="step.settings"\
                        @validationComplete ="onValidationComplete"\
                    ></component>\
                </template>\
                <div\
                    :class  ="prefix + \'-validator-wrapper\'"\
                    :id     ="prefix + \'-validator\'">\
                    <div\
                        v-if    ="stepValid && stepFinal"\
                        :class  ="prefix + \'-validator-modal-window\'">\
                        <cjaf-base :content = "content.validator.window.modal.text"></cjaf-base>\
                        <cjaf-base\
                            :content        ="content.validator.window.modal.button.restart"\
                            @click.native   ="reset">\
                        </cjaf-base>\
                    </div>\
                    <div :class="prefix + \'-validator-widget-wrapper\'">\
                        <cjaf-base\
                            :content="content.validator.button[\
                                showPopUpWindow ? \'continue\' : \'validate\'\
                            ]"\
                            @click.native="onValidationButtonClick">\
                        </cjaf-base>\
                        <cjaf-base\
                            v-if        ="showPopUpWindow"\
                            :class      ="prefix + \'-validator-pop-up-window\'"\
                            :content    ="content.validator.window.popUp.text[\
                                stepValid ? \'correct\' : \'wrong\'\
                            ]">\
                        </cjaf-base>\
                    </div>\
                </div>\
            </div>\
        </div>\
    ',

    props: {
        content: {
            type: Object,
            required: true
        },
        prefix: {
            type: String,
            default: 'cjaf'
        }
    },
    data: function() {
        return {
            showPopUpWindow: false,
            stepValid: false,
            activeStep: 1
        }
    },
    computed: {
        stepsCount: function() {
            return this.content.steps.length;
        },
        stepLabel: function(i) {
            return [
                {
                    _tag: 'div',
                    _html: i.toString(),
                    _class: this.prefix + '-step-label-text'
                }
            ]
        },
        stepLabelClass: function(i) {
            var s = '';
            var ret = {};
            if (i === this.activeStep) {
                s += '-active';
            } else if (i < this.activeStep) {
                s += '-past'
            } else {
                s += '-inactive'
            }
            ret[this.prefix + '-step-label' + s] = true;
            return ret;

        },
        stepFinal: function() {
            return this.activeStep === this.stepsCount;
        }
    },
    watch: {
        activeStep: function() {
            this.$nextTick(function() {
                this.$refs['step' + this.activeStep].reset();
            })
        }
    },
    methods: {
        validate: function() {
            this.$refs['step' + this.activeStep][0].validate();
        },
        reset: function() {
            this.stepValid = false;
            this.$refs['step' + this.activeStep][0].reset();
        },
        onValidationComplete: function(status) {
            this.stepValid = status;
            this.showPopUpWindow = !this.stepFinal || (this.stepFinal && !this.stepValid);
        },
        onValidationButtonClick: function() {
            if (this.showPopUpWindow) {
                this.showPopUpWindow = false;
                this.reset();
            } else {
                this.validate();
            }
        }
    }
});