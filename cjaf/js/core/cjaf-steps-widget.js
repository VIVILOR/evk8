Vue.component('cjaf-steps-widget', {
    template: '\
        <div\
            id="prefix + \'-steps-widget\'"\
            class="prefix + \'-steps-widget\'"\
        >\
            <ul\
                v-if="stepsCount > 1"\
            >\
                <li \
                    is          = "cjaf-base"\
                    v-for       = "i in content.length"\
                    :content    = "stepLabel(i)"\
                    :class      = "stepLabelClass(i)"\
                ></li>\
            </ul>\
        </div>\
    ',
    props: {

    },
    computed: {
        activeStep: function() {
            return 1;
        },
        stepsCount: function() {
            return this.content.length;
        },
        stepLabel: function(i) {
            return [
                {
                    _tag: 'div',
                    _html: i.toString(),
                    _class: this.prefix + '-step-label-text'
                }
            ]
        },
        stepLabelClass: function(i) {
            var s = '';
            var ret = {};
            if (i === this.activeStep) {
                s += '-active';
            } else if (i < this.activeStep) {
                s += '-past'
            } else {
                s += '-inactive'
            }
            ret[this.prefix + '-step-label' + s] = true;
            return ret;

    }
});