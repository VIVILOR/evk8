Vue.component('cjaf-validator', {
    template: '\
        <div\
            :class  = "prefix + \'-validator-wrapper\'"\
            :id     = "prefix + \'-validator\'"\
        >\
            <div\
                :class = "prefix + \'-validator-modal-window\'"\
            >\
                <template\
                    v-for = "part in reactiveContent.modalWindow.text"\
                >\
                    <cjaf-base\
                        :content = "part"\
                    ></cjaf-base>\
                </template>\
                <cjaf-base\
                    :content = "reactiveContent.modalWindow.button.restart"\
                    @click.native = "$parent.$refs.step0.reset()"\
                ></cjaf-base>\
            </div>\
            <div\
                :class = "prefix + \'-validator-widget-wrapper\'"\
            >\
                <cjaf-base\
                    :content        = "reactiveContent.button"\
                    @click.native   = "$parent.$refs.step0.validate()"\
                ></cjaf-base>\
                <div\
                    v-if="showWindow && ((!final) || (final && !valid))"\
                    :class="prefix + \'-validator-pop-up-window\'"\
                >\
                    <cjaf-base\
                        :visible    = "valid"\
                        :content    = "reactiveContent.popUpWindow.text.correct"\
                    ></cjaf-base>\
                    <cjaf-base\
                        :visible    = "!valid"\
                        :content    = "reactiveContent.popUpWindow.text.wrong"\
                    ></cjaf-base>\
                </div>\
            </div>\
        </div>\
    ',
    props: {
        content: {
            type: Object,
            required: true
        },
        prefix: {
            type: String,
            default: 'cjaf'
        },
        stepIndex: {
            type: Number,
            required: true
        },
        showWindow: {
            type: Boolean,
            default: false
        }
    },
    computed: {
        reactiveContent: function() {
            return this.content;
        },
        valid: function() {
            return this.$parent['step' + this.stepIndex].completed;
        },
        final: function() {
            return this.$parent['steps-widget'].currentStep === this.$parent['steps-widget'].activeStep;
        }
    },
    methods: {
    }
});