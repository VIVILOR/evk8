var cjaf = {
    prefix: "cjaf",
    mixins: {
        'label-swap': {
            mounted: function() {

            }
        }
    },
    utils: {
        compareArrays: function (ops) {
            var i = 0;

            if (ops.isBinary) {
                console.log('Binary array');
                for (i = 0; i < ops.current.length - 1; i++) {
                    if (ops.current[i] !== ops.current[i + 1]) {
                        console.log('c');
                        return false;
                    }
                }
                return true;
            } else {
                if (ops.correct) {
                    for (i in ops.current)
                        if (ops.current[i] !== ops.correct[i])
                            return false;
                    return true;
                }
            }
            return false;
        },
        fillArray: function (ops) {
            console.log(ops);
            if (!ops || !ops.len)
                return null;

            var ret = [];
            var i = 0;
            var divider = null;

            ret.length = ops.len;

            if (ops.maxValue)
                divider = ops.maxValue;
            else if (ops.isBinary)
                divider = 2;
            else
                divider = 10;

            console.log('Creating array with values in range [0:' + (divider - 1) + ']');
            for (i = 0; i < ret.length; i++) {
                ret[i] = (Math.random() * 100 | 0) % divider;
                if (i === ret.length - 1) {
                    if (this.compareArrays({
                        correct: ops.correct,
                        current: ret,
                        isBinary: ops.isBinary
                    })) {
                        i = 0;
                    }
                }
            }

            return ret;
        }
    }
};